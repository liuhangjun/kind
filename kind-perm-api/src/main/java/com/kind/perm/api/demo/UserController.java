package com.kind.perm.api.demo;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.kind.common.dto.DataGridResult;
import com.kind.common.persistence.PageView;
import com.kind.perm.api.common.controller.BaseController;
import com.kind.perm.core.demo.domain.CommunityDO;
import com.kind.perm.core.demo.service.CommunityService;
import com.kind.perm.core.system.service.CoFileService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * 由于功能都比较简单 故没有写service层
 *
 * @author hao.su
 */
@Api(description = "用户信息")
@RestController
@RequestMapping("/user")
public class UserController extends BaseController{
	
	
//	service的注入原来的套路
//	@Autowired
//	private CoFileService coFileService;
	@Autowired
	private CommunityService communityService;

	@ApiOperation(value = "查询用户", notes = "查询用户", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
	@RequestMapping(value = "/user.json", params = "name", method = RequestMethod.GET)
	@ResponseBody
	public Object selectUser(
			@ApiParam(required = true, value = "用户名") @RequestParam(required = true) String name) {
		System.out.println("param name:" + name);
		
		Map<String, Object> map = new HashMap<String,Object>();
		map.put("status", 0);
		map.put("data", "");
		map.put("message", null);
		return map;
	}
	
	/**
	 * 获取分页查询列表数据.
	 */
	@ApiOperation(value = "查询列表", notes = "查询列表", httpMethod = "GET", produces = MediaType.APPLICATION_JSON_VALUE)
	@RequestMapping(value = "selectPageList", method = RequestMethod.GET)
	@ResponseBody
	public DataGridResult selectPageList(CommunityDO query, HttpServletRequest request) {
		PageView<CommunityDO> page = communityService.selectPageList(query);
		return super.buildDataGrid(page);
	}

}
