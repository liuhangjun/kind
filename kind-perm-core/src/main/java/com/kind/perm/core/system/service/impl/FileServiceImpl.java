package com.kind.perm.core.system.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.kind.common.exception.ServiceException;
import com.kind.perm.core.system.dao.CoFileDao;
import com.kind.perm.core.system.dao.FileDao;
import com.kind.perm.core.system.domain.CoFileDO;
import com.kind.perm.core.system.domain.FileDO;
import com.kind.perm.core.system.service.FileService;

/**
 * 
 * 小区管理业务处理实现类. <br/>
 *
 * @date:2016年12月12日 <br/>
 * @author 李明
 * @version:
 * @since:JDK 1.7
 */
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class FileServiceImpl implements FileService {

	@Autowired
	private FileDao fileDao;
	
	@Autowired
	private CoFileDao cofileDao;

	
	@Override
	public int save(FileDO entity) throws ServiceException {
		try {
			return fileDao.saveOrUpdate(entity);

        }catch (Exception e){
		    e.printStackTrace();
            throw new ServiceException(e.getMessage());
        }

	}


	@Override
	public void remove(Long id) {
		 try {
			 fileDao.remove(id);
			 Map map = new HashMap();
			 map.put("objectAId", id);
			 cofileDao.deleteByMap(map);

	        }catch (Exception e){
	            throw new ServiceException(e.getMessage());
	        }
	}
	@Override
	public FileDO getById(Long id) {
		return fileDao.getById(id);
	}
	
	@Override
	public List<FileDO> getFileDOlistByCoFileDO(CoFileDO entity){
	
		try {
			List<CoFileDO> cofileDOlist =cofileDao.getCoFileDOListByTypeBAndTypeBId(entity.getObjectA(), entity.getObjectB(), entity.getObjectBId());
			List<FileDO> fileDOlist = new ArrayList<FileDO>();
			for (CoFileDO coFileDO : cofileDOlist) {
				FileDO filedo = new FileDO();
				filedo = fileDao.getById(coFileDO.getObjectAId());
				fileDOlist.add(filedo);
			}
			return fileDOlist;
			
			
	        }catch (Exception e){
	            throw new ServiceException(e.getMessage());
	        }
	}
}
